﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using IniParser;
using IniParser.Model;

namespace ImageIndexer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Dice> diceList;
        public List<string> diceCSVRecords;
        public string[] images;
        public string imageDir;
        public int activeImageIndex;
        public int autoSkipCount;
        public double dieWidth;
        public BitmapImage loadedImage;

        public MainWindow()
        {
            InitializeComponent();

            this.activeImageIndex = 0;
            this.diceList = new List<Dice>();
            this.diceCSVRecords = new List<string>();

            // Configurable parameters
            this.imageDir = "/";
            this.dieWidth = 75.0;
            this.autoSkipCount = 3;

            if (System.IO.File.Exists("config.ini"))
            {
                try
                {
                    FileIniDataParser parser = new FileIniDataParser();
                    IniData data = parser.ReadFile("config.ini");
                    this.imageDir = data["directories"]["source"].Trim();

                    if (!System.IO.Directory.Exists(this.imageDir))
                        this.imageDir = "\\";

                    if (!double.TryParse(data["misc"]["width"], out this.dieWidth))
                        this.dieWidth = 75;

                    if (!Int32.TryParse(data["misc"]["autoskip_objects"], out this.autoSkipCount))
                        this.autoSkipCount = 0;
                }
                catch (Exception e)
                {
                    this.imageDir = "/";
                    this.dieWidth = 75.0;
                    this.autoSkipCount = 3;

                    this.AddHistory($"Error reading config: {e.Message}");
                }
            }
            this.images = System.IO.Directory.GetFiles(this.imageDir, "*.jpg");

            AddHistory($"Found {this.images.Length.ToString()} images");

            ChangeImage();
        }
        
        private void Canvas_pick_KeyUp(object sender, KeyEventArgs e)
        {
            Point p = Mouse.GetPosition(this);
            double x = (p.X - this.canvas_pick.Margin.Left) - this.dieWidth / 2.0;
            double y = (p.Y - this.canvas_pick.Margin.Top) - this.dieWidth / 2.0;

            switch (e.Key)
            {
                case Key.D1:
                    AddDie(x, y, 1);
                    if (this.diceList.Count == this.autoSkipCount && this.autoSkipCount > 0)
                        NextImage();
                    break;
                case Key.D2:
                    AddDie(x, y, 2);
                    if (this.diceList.Count == this.autoSkipCount && this.autoSkipCount > 0)
                        NextImage();
                    break;
                case Key.D3:
                    AddDie(x, y, 3);
                    if (this.diceList.Count == this.autoSkipCount && this.autoSkipCount > 0)
                        NextImage();
                    break;
                case Key.D4:
                    AddDie(x, y, 4);
                    if (this.diceList.Count == this.autoSkipCount && this.autoSkipCount > 0)
                        NextImage();
                    break;
                case Key.D5:
                    AddDie(x, y, 5);
                    if (this.diceList.Count == this.autoSkipCount && this.autoSkipCount > 0)
                        NextImage();
                    break;
                case Key.D6:
                    AddDie(x, y, 6);
                    if (this.diceList.Count == this.autoSkipCount && this.autoSkipCount > 0)
                        NextImage();
                    break;
                case Key.Q:
                    SaveMasterCSV();
                    break;

                // Remove last entry
                case Key.R:
                case Key.Delete:
                case Key.Back:
                    RemoveDie();
                    break;

                case Key.Left:
                case Key.A:
                    PreviousImage();
                    break;

                case Key.Right:
                case Key.D:
                    NextImage();
                    break;
            }
        }

        private void PreviousImage()
        {
            // save, open previous file
            this.SaveOutlines();
            
            this.AddHistory("Previous Image");
            foreach (Dice d in diceList)
            {
                this.diceCSVRecords.Add(d.toCSV(System.IO.Path.GetFileName(this.images[this.activeImageIndex]),
                    ((int)Math.Ceiling(this.loadedImage.Width)),
                    ((int)Math.Ceiling(this.loadedImage.Height))
                ));
            }
            this.activeImageIndex = Math.Max(0, this.activeImageIndex - 1);
            ChangeImage();
        }

        private void NextImage()
        {
            // save, open next file
            this.SaveOutlines();
            
            this.AddHistory("Next Image");
            foreach (Dice d in diceList)
            {
                this.diceCSVRecords.Add(d.toCSV(System.IO.Path.GetFileName(this.images[this.activeImageIndex]),
                    ((int)Math.Ceiling(this.loadedImage.Width)),
                    ((int)Math.Ceiling(this.loadedImage.Height))
                ));
            }
            this.activeImageIndex = Math.Min(this.images.Length - 1, this.activeImageIndex + 1);
            ChangeImage();
        }

        private void SaveOutlines()
        {
            if (this.diceList.Count > 0)
            {
                this.AddHistory("Save Image XML");

                int width = ((int)Math.Ceiling(this.loadedImage.Width));
                int height = ((int)Math.Ceiling(this.loadedImage.Height));
                int depth = 3;  // wtf
                string folder = string.Empty;
                string filename = string.Empty;
                string path = string.Empty;

                path = this.images[this.activeImageIndex];
                filename = System.IO.Path.GetFileName(this.images[this.activeImageIndex]);
                string[] tmp = this.images[this.activeImageIndex].Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
                if (tmp.Length > 2)
                    folder = tmp[tmp.Length - 2];

                string masterXml = "<annotation>" +
                    $"<folder>{folder}</folder>" +
                    $"<filename>{filename}</filename>" +
                    $"<path>{path}</path>" +
                    "<source><database>Unknown</database></source>" +
                    $"<size><width>{width}</width><height>{height}</height><depth>{depth}</depth></size>" +
                    "<segmented>0</segmented>" +
                    string.Join(" ", this.diceList) +
                    "</annotation>";
                System.IO.File.WriteAllText(this.images[this.activeImageIndex].Replace(".jpg", ".xml"), masterXml);
            }
        }

        private void SaveMasterCSV()
        {
            this.AddHistory("Saved CSV");

            List<string> lines = new List<string>();
            lines.Add("filename,width,height,class,xmin,ymin,xmax,ymax");   // Header
            lines.AddRange(this.diceCSVRecords);

            System.IO.File.WriteAllText("train.csv", string.Join("\r\n", lines));
        }

        private void ChangeImage()
        {
            if (images.Length <= 0)
                return;

            this.diceList = new List<Dice>();
            this.canvas_pick.Children.Clear();

            var uri = new Uri(System.IO.Path.Combine(imageDir, images[activeImageIndex]));
            this.loadedImage = new BitmapImage(uri);
            this.img_pick.Source = this.loadedImage;

            string xmlFile = System.IO.Path.Combine(imageDir, images[activeImageIndex]).Replace(".jpg", ".xml");
            if (System.IO.File.Exists(xmlFile))
            {
                string xmlData = System.IO.File.ReadAllText(xmlFile);
                // TODO: Load this stuff.. need to parse XMLx
                System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                doc.LoadXml(xmlData);
                System.Xml.XmlNodeList nodes = doc.SelectNodes("/annotation/object");
                for (int i = 0; i < nodes.Count; i++)
                {
                    System.Xml.XmlDocument objectXml = new System.Xml.XmlDocument();
                    objectXml.LoadXml("<object>" + nodes.Item(i).InnerXml + "</object>");

                    string pipCountStr = objectXml.SelectSingleNode("//name").InnerText;
                    int pipCount = -1;
                    double minX = double.Parse(objectXml.SelectSingleNode("//xmin").InnerText);
                    double minY = double.Parse(objectXml.SelectSingleNode("//ymin").InnerText);
                    double maxX = double.Parse(objectXml.SelectSingleNode("//xmax").InnerText);
                    double maxY = double.Parse(objectXml.SelectSingleNode("//ymax").InnerText);

                    switch (pipCountStr)
                    {
                        case "one": pipCount = 1; break;
                        case "two": pipCount = 2; break;
                        case "three": pipCount = 3; break;
                        case "four": pipCount = 4; break;
                        case "five": pipCount = 5; break;
                        case "six": pipCount = 6; break;
                        default: pipCount = 1; break;
                    }

                    this.AddDie(minX, minY, pipCount);
                }
            }

            this.Title = $"{activeImageIndex + 1}/{images.Length} - {images[activeImageIndex]}";
        }

        private void RemoveDie()
        {
            if (this.diceList.Count > 0)
            {
                this.diceList.RemoveAt(this.diceList.Count - 1);
            }
            if (this.canvas_pick.Children.Count > 0)
            {
                this.canvas_pick.Children.RemoveAt(this.canvas_pick.Children.Count - 1);    // text
                this.canvas_pick.Children.RemoveAt(this.canvas_pick.Children.Count - 1);    // rectangle
                this.canvas_pick.Children.RemoveAt(this.canvas_pick.Children.Count - 1);    // rectangle2

                this.AddHistory("Delete die");
            }
        }

        private void AddHistory(string msg)
        {
            this.txt_history.Text = msg + "\r\n" + this.txt_history.Text;
        }
        
        private void AddDie(double x, double y, int pipCount)
        {
            this.diceList.Add(new Dice(x, y, pipCount, this.dieWidth));

            this.AddHistory("Add " + pipCount.ToString());

            SolidColorBrush bg = new SolidColorBrush(Colors.White);
            SolidColorBrush fg = new SolidColorBrush(Colors.Black);

            switch (pipCount)
            {
                case 1:
                    fg = new SolidColorBrush(Colors.Orange);
                    bg = new SolidColorBrush(Colors.Black);
                    break;
                case 2:
                    fg = new SolidColorBrush(Colors.White);
                    bg = new SolidColorBrush(Colors.Black);
                    break;
                case 3:
                    fg = new SolidColorBrush(Colors.Green);
                    break;
                case 4:
                    fg = new SolidColorBrush(Colors.Blue);
                    break;
                case 5:
                    fg = new SolidColorBrush(Colors.Yellow);
                    bg = new SolidColorBrush(Colors.Black);
                    break;
                case 6:
                    fg = new SolidColorBrush(Colors.Magenta);
                    break;
                default:
                    fg = new SolidColorBrush(Colors.Red);
                    break;
            }

            Rectangle rec2 = new Rectangle();
            Canvas.SetLeft(rec2, x);
            Canvas.SetTop(rec2, y);
            rec2.Width = this.dieWidth;
            rec2.Height = this.dieWidth;
            rec2.Fill = fg;
            rec2.Opacity = 0.25;
            this.canvas_pick.Children.Add(rec2);

            Rectangle rec = new Rectangle();
            Canvas.SetLeft(rec, x);
            Canvas.SetTop(rec, y);
            rec.Width = this.dieWidth;
            rec.Height = this.dieWidth;
            rec.Fill = new SolidColorBrush(Colors.Transparent);

            rec.Stroke = fg;
            rec.Opacity = 0.75;

            rec.StrokeThickness = 2.0;
            this.canvas_pick.Children.Add(rec);

            TextBlock tmptxt = new TextBlock()
            {
                Text = pipCount.ToString(),
                Foreground = bg,
                Background = fg,
                FontSize = 22.0,
                Margin = new Thickness(x, y, 0, 0)
            };
            this.canvas_pick.Children.Add(tmptxt);
        }

        private void Img_pick_MouseMove(object sender, MouseEventArgs e)
        {
            this.txt_a.Text = Mouse.GetPosition(this).X.ToString("F2");
            this.txt_b.Text = Mouse.GetPosition(this).Y.ToString("F2");
        }
    }

    public class Dice
    {
        public int x;
        public int y;
        public int width;
        public int pipCount;
        public string label;

        public Dice(double x, double y, int pips, double dw)
        {
            this.x = (int)Math.Ceiling(x);
            this.y = (int)Math.Ceiling(y);
            this.pipCount = pips;
            this.width = (int)Math.Ceiling(dw);

            switch (pips)
            {
                case 1: this.label = "one";   break;
                case 2: this.label = "two";   break;
                case 3: this.label = "three"; break;
                case 4: this.label = "four";  break;
                case 5: this.label = "five";  break;
                case 6: this.label = "six";   break;
            }
        }

        public string toCSV(string filename, int imageWidth, int imageHeight)
        {
            // filename,width,height,class,xmin,ymin,xmax,ymax
            List<string> parts = new List<string>();
            parts.Add(filename);
            parts.Add(imageWidth.ToString());
            parts.Add(imageHeight.ToString());
            parts.Add(this.label);
            parts.Add(this.x.ToString());
            parts.Add(this.y.ToString());
            parts.Add((this.x + this.width).ToString());
            parts.Add((this.y + this.width).ToString());

            return string.Join(",", parts);
        }

        public override string ToString()
        {
            return "<object>" + 
                $"<name>{this.label}</name>" +
                "<pose>Unspecified</pose>" +
                "<truncated>0</truncated>" +
                "<difficult>0</difficult>" +
                "<bndbox>" +
                $"<xmin>{this.x}</xmin>" +
                $"<ymin>{this.y}</ymin>" +
                $"<xmax>{this.x + this.width}</xmax>" +
                $"<ymax>{this.y + this.width}</ymax>" +
                "</bndbox>" +
                "</object>";
        }
    }
}
