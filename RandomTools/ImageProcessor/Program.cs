﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImageProcessor
{
    class Program
    {
        public struct Coord
        {
            public int x;
            public int y;

            public Coord(int _x, int _y)
            {
                this.x = _x;
                this.y = _y;
            }
        }

        static void Main(string[] args)
        {
            string[] images = Directory.GetFiles("C:\\users\\jack\\pictures\\dice", "*.jpg");

            Program.LoadImage(images[2]);
            
            foreach (string image in images) {
                Program.LoadImage(image);
            }
            
            Console.WriteLine("Done processing...");
        }

        public static void LoadImage(string fileName)
        {
            Bitmap imageRGB = new Bitmap(fileName);
            //imageRGB.Save("out\\original.jpg", ImageFormat.Jpeg);

            /*
            Bitmap imageGrey = MakeGrayscale(imageRGB);
            imageGrey.Save("out\\grey.jpg", ImageFormat.Jpeg);

            Bitmap imageRed = MakeRedScale(imageRGB);
            imageRed.Save("out\\red.jpg", ImageFormat.Jpeg);
            */

            try
            {
                int subCount = 0;
                int buffer = 15;
                foreach (Rectangle r in GetRectangles(imageRGB))
                {
                    Bitmap tempImg = new Bitmap(r.Width + buffer * 2, r.Height + buffer * 2);

                    for (int x = 0; x < (r.Right - r.Left) + buffer * 2; x++)
                    {
                        for (int y = 0; y < (r.Bottom - r.Top) + buffer * 2; y++)
                        {
                            tempImg.SetPixel(x, y, imageRGB.GetPixel(r.Left + x - buffer, r.Top + y - buffer));
                        }
                    }

                    tempImg.Save("out\\all\\" + System.IO.Path.GetFileNameWithoutExtension(fileName) + "_" + (subCount++) + ".png", ImageFormat.Png);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Processing {fileName} threw exception: {e.Message}");
            }

            //Bitmap imageJackRed = MakeJackRed(imageRGB);
            //imageJackRed.Save("out\\all\\" + System.IO.Path.GetFileNameWithoutExtension(fileName) + ".png", ImageFormat.Png);
            //imageJackRed.Save("out\\jack-red.png", ImageFormat.Png);
        }

        public static Rectangle[] GetRectangles(Bitmap original)
        {
            // Initialize
            List<Rectangle> subImages = new List<Rectangle>();
            
            int stepSize = 25;
            List<Coord> hits = new List<Coord>();
            for (int x = 0; x < original.Width; x += stepSize)
            {
                for (int y = 0; y < original.Height; y += stepSize)
                {
                    if (FoundRed(original.GetPixel(x, y)))
                    {
                        hits.Add(new Coord(x, y));
                    }
                }
            }

            for (var i = 0; i < hits.Count; i++)
            {
                bool process = true;
                foreach (Rectangle r in subImages)
                {
                    if (r.Left <= hits[i].x && r.Right >= hits[i].x &&
                        r.Top <= hits[i].y && r.Bottom >= hits[i].y)
                    {
                        process = false;
                    }
                }

                if (process)
                {
                    List<Coord> currentBlock = new List<Coord>();
                    currentBlock.Add(hits[i]);
                    FindAllNeighbors(original, hits[i], currentBlock);
                    int maxX = -1;
                    int maxY = -1;
                    int minX = original.Width + 1;
                    int minY = original.Height + 1;
                    foreach (Coord cTmp in currentBlock)
                    {
                        maxX = Math.Max(maxX, cTmp.x);
                        maxY = Math.Max(maxY, cTmp.y);
                        minX = Math.Min(minX, cTmp.x);
                        minY = Math.Min(minY, cTmp.y);
                    }
                    Rectangle rTmp = new Rectangle(minX, minY, (maxX - minX), (maxY - minY));
                    subImages.Add(rTmp);
                }
            }

            return subImages.ToArray();
        }

        public static void FindAllNeighbors(Bitmap img, Coord currentCoord, List<Coord> currentBlock)
        {
            int x = currentCoord.x;
            int y = currentCoord.y;
            // x, y - 1  (up)
            Coord c = new Coord(x, y - 1);
            if (!currentBlock.Contains(c) && FoundRed(img.GetPixel(x, y - 1)))
            {
                currentBlock.Add(c);
                FindAllNeighbors(img, c, currentBlock);
            }

            // x, y + 1 (down)
            c = new Coord(x, y + 1);
            if (!currentBlock.Contains(c) && FoundRed(img.GetPixel(x, y + 1)))
            {
                currentBlock.Add(new Coord(x, y + 1));
                FindAllNeighbors(img, c, currentBlock);
            }

            // x - 1, y (left)
            c = new Coord(x - 1, y);
            if (!currentBlock.Contains(c) && FoundRed(img.GetPixel(x - 1, y)))
            {
                currentBlock.Add(new Coord(x - 1, y));
                FindAllNeighbors(img, c, currentBlock);
            }

            // x + 1, y (right)
            c = new Coord(x + 1, y);
            if (!currentBlock.Contains(c) && FoundRed(img.GetPixel(x + 1, y)))
            {
                currentBlock.Add(new Coord(x + 1, y));
                FindAllNeighbors(img, c, currentBlock);
            }

        }

        public static bool FoundRed(Color c)
        {
            if (c.R > (c.G + c.B))
                return true;
            return false;
        }

        public static Bitmap MakeJackRed(Bitmap original)
        {
            int threshold = 150;
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);
            for (int x = 0; x < newBitmap.Width; x++)
            {
                for (int y = 0; y < newBitmap.Height; y++)
                {
                    int r = original.GetPixel(x, y).R;
                    int g = original.GetPixel(x, y).G;
                    int b = original.GetPixel(x, y).B;
                    //if (original.GetPixel(x, y).R > threshold && original.GetPixel(x,y).G < threshold * 0.8 && original.GetPixel(x,y).B < threshold * 0.8)
                    if (r > (g + b))
                        newBitmap.SetPixel(x, y, Color.Red);
                    else
                        newBitmap.SetPixel(x, y, Color.Black);
                }
            }
            return newBitmap;
        }

        public static Bitmap MakeRedScale(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            ColorMatrix myColorMatrix = new ColorMatrix();

            // Red
            myColorMatrix.Matrix00 = 1.00f;
            myColorMatrix.Matrix11 = 0.00f;
            myColorMatrix.Matrix22 = 0.00f;
            myColorMatrix.Matrix33 = 1.00f;
            myColorMatrix.Matrix44 = 1.00f;


            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(myColorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }

        public static Bitmap MakeGrayscale(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(new float[][]
            {
                new float[] {.3f, .3f, .3f, 0, 0},
                new float[] {.59f, .59f, .59f, 0, 0},
                new float[] {.11f, .11f, .11f, 0, 0},
                new float[] {0, 0, 0, 1, 0},
                new float[] {0, 0, 0, 0, 1}
            });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }
    }
}