﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpandLabelBounds
{
    class Program
    {
        static void Main(string[] args)
        {
            // The new size = width + (2 * adjustment) -- the adjustment is applied to max and min of both x and y
            int adjustment = 3;
            string sourceDirectory = System.IO.Directory.GetCurrentDirectory();
            string sourcePattern = "*.csv";
            string runTime = DateTime.UtcNow.ToString("yyyyMMddhhmmss");
            string[] files = System.IO.Directory.GetFiles(sourceDirectory, sourcePattern, System.IO.SearchOption.TopDirectoryOnly);
            foreach (string file in files)
            {
                System.IO.File.Copy(file, file + "." + runTime, true);
            }

            foreach (string file in files)
            {
                string[] lines = System.IO.File.ReadAllLines(file + "." + runTime);
                List<string> newLines = new List<string>();
                
                foreach (string line in lines)
                {
                    // filename,width,height,class,xmin,ymin,xmax,ymax
                    // 0       ,1    ,2     ,3    ,4   ,5   ,6   ,7
                    string[] parts = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    if (newLines.Count > 0)
                    {
                        if (parts.Length == 8)
                        {
                            double.TryParse(parts[1], out double imgWidth);
                            double.TryParse(parts[2], out double imgHeight);
                            double.TryParse(parts[4], out double xmin);
                            double.TryParse(parts[5], out double ymin);
                            double.TryParse(parts[6], out double xmax);
                            double.TryParse(parts[7], out double ymax);

                            parts[4] = Math.Max(0, (xmin - adjustment)).ToString();
                            parts[5] = Math.Max(0, (ymin - adjustment)).ToString();
                            parts[6] = Math.Min((xmax + adjustment), imgWidth - 1).ToString();
                            parts[7] = Math.Min((ymax + adjustment), imgHeight - 1).ToString();
                        }
                    }

                    newLines.Add(string.Join(",", parts));
                }
                System.IO.File.WriteAllLines(file, newLines.ToArray());
            }
        }
    }
}
