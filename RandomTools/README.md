These are a few quick and dirty tools. They are all C#, written in windows on .Net 4.7.2. 
C# was chosen for ease and speed of development.
# ExpandLableBounds
* This project will open a series of .csv files and exapnd the image bounds by 'adjustment'
* The CSV is expected to have the following format:
  *  // filename,width,height,class,xmin,ymin,xmax,ymax
# ImageIndexer
* This project opens a directory full of .jpg files (and related .xml files if they exist)
* An image is shown to the user. The user moves the mouse and presses the key
* when hovering of the center of the object they want to tag
* By default, after three objects are added for the image the next image is loaded
* Whenever the image is changed the current object selections are saved 
* When opening a new jpg if there is a .xml with the same name it will open that
# ImageProcessor
* Opens a series of .jpg files and produces up to three new images based on areas of detected 'red'
 